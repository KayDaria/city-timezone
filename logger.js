import winston       from 'winston';
import moment        from 'moment';
import fs            from 'fs';
import winstonRotate from 'winston-daily-rotate-file';

winston.emitErrs = true;

const logDir = `${__dirname}/logs`;

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const log = new (winston.Logger)({
  transports: [
    new (winstonRotate)({
      level:            'debug',
      filename:         `${logDir}/%DATE%_app.log`,
      handleExceptions: false,
      json:             true,
      maxsize:          5242880, // 5MB
      maxFiles:         5,
      colorize:         false,
      timestamp:        () => moment().format(moment.defaultFormat)
    }),
    new (winston.transports.Console)({
      level:            'debug',
      handleExceptions: false,
      json:             false,
      colorize:         true
    })
  ],
  exitOnError: false
});

export default log;
