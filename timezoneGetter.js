import { getCoordinates } from './services/yandexService';
import { getTimezone }    from './services/googleService';

/**
 * Метод валидации переданного параметра города
 *
 * @param {string} city - название города
 * @returns {(boolean|string)} - корректное название города или false в случае ошибки
 */
function cityValidation(city) {
  if (!city || typeof city !== 'string') return false;

  const regexp = /[-а-яА-Я0-9a-zA-Z ]+/;
  const check = regexp.exec(city);

  if (!check) return false;

  return check[ 0 ];
}

/**
 * Метод получения таймзоны переданного города через API Яндекса и Google
 *
 * @param {String} city - название города
 * @returns {Object} - идентификатор и название часового пояса
 */
async function timezoneGetter(city) {
  city = cityValidation(city);

  if (!city) return {};

  let coordinates = await getCoordinates(city);

  if (!coordinates) return {};

  return await getTimezone(coordinates);
}

export default timezoneGetter;