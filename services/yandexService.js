import MultiGeocoder  from 'multi-geocoder';
import retry          from 'async-retry';

import logger         from '../logger';
import { RETRY_OPTS } from '../config';

const geocoder = new MultiGeocoder({
  provider:   'yandex-cache',
  coordorder: 'latlong'
});

/**
 * Метод получения координат города по его названию
 *
 * @param {string} city - название города
 * @returns {Array|boolean} - массив из двух элементов [latitude, longitude] или false в случае ошибки
 */
export async function getCoordinates(city) {
  try {
    let response = await retry(async () => await geocoder.geocode([ city ]), RETRY_OPTS );

    if (response.errors && response.errors[0]) {
      const message = response.errors.reduce((message, error) => message += `${error.reason}\n`, '');

      throw new Error(message);
    }

    const coordinates = response.result.features.map(item => item.geometry.coordinates);

    return coordinates.length ? coordinates[ 0 ] : false;
  } catch (err) {
    logger.error(`Ошибка при работе с Яндекс API: ${err}`);

    return false;
  }
}
