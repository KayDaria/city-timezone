import googleMaps     from '@google/maps';

import { GOOGLE_KEY } from '../config';
import logger         from '../logger';

const client = googleMaps.createClient({
  key:      GOOGLE_KEY,
  language: 'ru',
  Promise:  Promise
});

/**
 * Метод получения таймзоны для точки, заданной парой широты / долготы
 *
 * @param {Array} location - массив из двух элементов [latitude, longitude]
 * @returns {Object} - состоит из идентификатора "tz" и полного названия часовой зоны
 */
export async function getTimezone(location) {
  try {
    const response = await client.timezone({ location }).asPromise();

    if (response.json.status !== 'OK') {
      const message = `Status: ${response.status} - ${response.error_message}`;

      throw new Error(message);
    }

    return {
      id:   response.json.timeZoneId,
      name: response.json.timeZoneName
    };
  } catch (err) {
    logger.error(`Ошибка при работе с Google API: ${err}`);

    return {};
  }
}