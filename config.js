/**
 * Ключ API для использования Google Maps Time Zone API
 *
 * @type {string}
 */
export const GOOGLE_KEY = 'AIzaSyBdwuRuGg48OTXuxkrodihclXeZZd4gBOo';

/**
 * Настройки для функции повтора запроса к сервису
 *
 * @type {{retries: number, maxTimeout: number, randomize: boolean}}
 */
export const RETRY_OPTS = {
  retries:    5,
  maxTimeout: 300000,
  randomize:  true
};