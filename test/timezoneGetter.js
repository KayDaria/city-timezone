import assert         from 'assert';

import timezoneGetter from '../timezoneGetter';

describe('Получим таймзоны', () => {
  it('Передадим Красноярск - получим корректный объект таймзоны', async () => {
    return timezoneGetter('Красноярск')
      .then(tz => {
        assert.deepEqual(tz, { id: 'Asia/Krasnoyarsk', name: 'Красноярск, стандартное время'});
      });
  });

  it('Передадим массив - получим пустой объект таймзоны', async () => {
    return timezoneGetter([ 'Красноярск' ])
      .then(tz => {
        assert.deepEqual(tz, {});
      });
  });

  it('Передадим Томск 7 - получим корректный объект таймзоны', async () => {
    return timezoneGetter('Томск 7')
      .then(tz => {
        assert.deepEqual(tz, { id: 'Asia/Novosibirsk', name: 'Новосибирск, стандартное время' });
      });
  });

  it('Передадим ничего - получим пустой объект таймзоны', async () => {
    return timezoneGetter('')
      .then(tz => {
        assert.deepEqual(tz, {});
      });
  });

  it('Передадим строку 123 - получим пустой объект таймзоны', async () => {
    return timezoneGetter('123')
      .then(tz => {
        assert.deepEqual(tz, {});
      });
  });

  it('Передадим число 123 - получим пустой объект таймзоны', async () => {
    return timezoneGetter(123)
      .then(tz => {
        assert.deepEqual(tz, {});
      });
  });

  it('Передадим объект - получим пустой объект таймзоны', async () => {
    return timezoneGetter({ name: 'Moscow' })
      .then(tz => {
        assert.deepEqual(tz, {});
      });
  });

  it('Передадим Краснорск (ошибочное написание) - получим корректный объект таймзоны', async () => {
    return timezoneGetter('Краснорск')
      .then(tz => {
        assert.deepEqual(tz, { id: 'Asia/Krasnoyarsk', name: 'Красноярск, стандартное время'});
      });
  });
});